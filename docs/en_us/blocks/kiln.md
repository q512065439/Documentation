The Kiln is a multiblock structure that is used as an in-world crafting device for many advanced materials.

The fundamental requirements to form are Kiln are as follows:
 * **Four** of any valid Brick Block (only Vanilla clay bricks by default).  
 * The first block **must** be over a valid heat source, Fire or Stoked Fire.  
 * The first block **must** have an air block directly above it  
 * The other three must surround that air block in any of the possible permutations  
 
 Here is an example of some kiln structures:
 ![Examples](betterwithmods:kilns.png)

The Kiln can be used for the creation of:

* [Firing Pottery](unfired_pottery.md)
* Charcoal from logs
* Baking!
* Smelting Ores
* Processing [Endstone](white_stone.md)